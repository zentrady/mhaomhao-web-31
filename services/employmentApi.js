import HttpRequest from './http_request'

export default class EmploymentAPI extends HttpRequest {
  getEmployments (params, auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/employment/', params)
  }
  getEmployment (id, auth) {
    this.setHeaderAuth(auth)
    return this.fetch(`/employment/${id}/`)
  }
  getMyEmployment (auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/employment/me/')
  }
  createEmployment (body, auth) {
    this.setHeaderAuth(auth)
    return this.createMultipart(`/employment/me/`, body)
  }
}
